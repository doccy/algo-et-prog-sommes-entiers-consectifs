/******************************************************************************
*                                                                             *
*  Copyright © 2016-2021 -- DoccY's Production                                *
*                                                                             *
*  Auteurs/Authors: Alban MANCHERON <doccy@mancheron.fr>                      *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  Ce fichier fait partie de mes ressources pédagogiques.                     *
*                                                                             *
*  Ce logiciel est régi par la   licence CeCILL soumise au droit français et  *
*  respectant les principes  de diffusion des logiciels libres.  Vous pouvez  *
*  utiliser, modifier et/ou redistribuer ce programme sous les conditions de  *
*  la licence CeCILL telle que diffusée par   le CEA, le CNRS et l'INRIA sur  *
*  le site "http://www.cecill.info".                                          *
*                                                                             *
*  En contrepartie de l'accessibilité au code source et des droits de copie,  *
*  de modification et de redistribution accordés par cette licence, il n'est  *
*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  *
*  seule une responsabilité  restreinte pèse  sur l'auteur du programme,  le  *
*  titulaire des droits patrimoniaux et les concédants successifs.            *
*                                                                             *
*  À  cet égard  l'attention de  l'utilisateur est  attirée sur  les risques  *
*  associés au   chargement, à   l'utilisation, à  la modification  et/ou au  *
*  développement et   à la reproduction du  logiciel par l'utilisateur étant  *
*  donné  sa spécificité  de logiciel libre,  qui peut le rendre  complexe à  *
*  manipuler et qui le réserve donc à des développeurs et des professionnels  *
*  avertis  possédant  des  connaissances  informatiques  approfondies.  Les  *
*  utilisateurs sont   donc invités   à charger   et tester  l'adéquation du  *
*  logiciel à   leurs besoins  dans des  conditions permettant  d'assurer la  *
*  sécurité de leurs systèmes et ou de leurs données et, plus  généralement,  *
*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.         *
*                                                                             *
*  Le fait que   vous puissiez accéder à cet  en-tête signifie que vous avez  *
*  pris connaissance de la licence CeCILL,   et que vous en avez accepté les  *
*  termes.                                                                    *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  This file is part of my educational resources.                             *
*                                                                             *
*  This software is governed by the CeCILL license under French law and       *
*  abiding by the rules of distribution of free software. You can use,        *
*  modify and/ or redistribute the software under the terms of the CeCILL     *
*  license as circulated by CEA, CNRS and INRIA at the following URL          *
*  "http://www.cecill.info".                                                  *
*                                                                             *
*  As a counterpart to the access to the source code and rights to copy,      *
*  modify and redistribute granted by the license, users are provided only    *
*  with a limited warranty and the software's author, the holder of the       *
*  economic rights, and the successive licensors have only limited            *
*  liability.                                                                 *
*                                                                             *
*  In this respect, the user's attention is drawn to the risks associated     *
*  with loading, using, modifying and/or developing or reproducing the        *
*  software by the user in light of its specific status of free software,     *
*  that may mean that it is complicated to manipulate, and that also          *
*  therefore means that it is reserved for developers and experienced         *
*  professionals having in-depth computer knowledge. Users are therefore      *
*  encouraged to load and test the software's suitability as regards their    *
*  requirements in conditions enabling the security of their systems and/or   *
*  data to be ensured and, more generally, to use and operate it in the same  *
*  conditions as regards security.                                            *
*                                                                             *
*  The fact that you are presently reading this means that you have had       *
*  knowledge of the CeCILL license and that you accept its terms.             *
*                                                                             *
******************************************************************************/

#include "fonctions.h"
#include "test_fonctions.h"
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>

/* Message rudimentaire pour l'utilisation du programme */
void usage(const char *msg, char *prog) {
  fprintf(stderr,
          "%sUsage : %s <Fonction> <a> <b> <nb_tests>\n\n"
          "Avec <Fonction> parmis 'V0', 'V1', 'V2' ou 'V3'\n",
          msg, basename(prog));
  exit(1);
}

/* Programme principal */
int main(int argc, char **argv) {

  char *endptr = NULL;
  unsigned int a, b;
  const char* fct_name = NULL;
  unsigned int nb_tests;
  SECG_t fct;
  infos_t infos;

  if (argc != 5) {
    usage("mauvaise utilisation!!!\n", argv[0]);
  }

  /* Le premier argument est le nom de la fonction */
  fct_name = argv[1];

  /* Le second argument est la valeur de a */
  a = strtoul(argv[2], &endptr, 10);
  if (endptr && (*endptr != '\0')) {
    usage("Erreur: le paramètre <n> doit être un entier naturel!\n", argv[0]);
  }

  /* Le troisième argument est la valeur de a */
  b = strtoul(argv[3], &endptr, 10);
  if (endptr && (*endptr != '\0')) {
    usage("Erreur: le paramètre <n> doit être un entier naturel!\n", argv[0]);
  }

  /* Le dernier argument est le nombre de tests à effectuer */
  nb_tests = strtoul(argv[4], NULL, 0);
  if (endptr && (*endptr != '\0')) {
    usage("Erreur: le paramètre <nb_tests> doit être un entier naturel!\n", argv[0]);
  }

  /* Selon le nom de fonction passé en paramètre, on assigne
     à la variable fct la fonction correspondante */
  if (strcmp(fct_name, "V0")) {
    if (strcmp(fct_name, "V1")) {
      if (strcmp(fct_name, "V2")) {
        if (strcmp(fct_name, "V3")) {
          fct = NULL;
          usage("Erreur: le paramètre <Fonction> n'est pas valide!\n", argv[0]);
        } else {
          fct = &SommeEntiersConsecutifsGeneraleV3;
        }
      } else {
        fct = &SommeEntiersConsecutifsGeneraleV2;
      }
    } else {
      fct = &SommeEntiersConsecutifsGeneraleV1;
    }
  } else {
    fct = &SommeEntiersConsecutifsGeneraleV0;
  }

  /* Test de la fonction */
  infos = test_fonction(fct, a, b, nb_tests);

  /* Affichage du résultat sur la sortie standard */
  print_infos(infos);

  return 0;
}
