/******************************************************************************
*                                                                             *
*  Copyright © 2016-2021 -- DoccY's Production                                *
*                                                                             *
*  Auteurs/Authors: Alban MANCHERON <doccy@mancheron.fr>                      *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  Ce fichier fait partie de mes ressources pédagogiques.                     *
*                                                                             *
*  Ce logiciel est régi par la   licence CeCILL soumise au droit français et  *
*  respectant les principes  de diffusion des logiciels libres.  Vous pouvez  *
*  utiliser, modifier et/ou redistribuer ce programme sous les conditions de  *
*  la licence CeCILL telle que diffusée par   le CEA, le CNRS et l'INRIA sur  *
*  le site "http://www.cecill.info".                                          *
*                                                                             *
*  En contrepartie de l'accessibilité au code source et des droits de copie,  *
*  de modification et de redistribution accordés par cette licence, il n'est  *
*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  *
*  seule une responsabilité  restreinte pèse  sur l'auteur du programme,  le  *
*  titulaire des droits patrimoniaux et les concédants successifs.            *
*                                                                             *
*  À  cet égard  l'attention de  l'utilisateur est  attirée sur  les risques  *
*  associés au   chargement, à   l'utilisation, à  la modification  et/ou au  *
*  développement et   à la reproduction du  logiciel par l'utilisateur étant  *
*  donné  sa spécificité  de logiciel libre,  qui peut le rendre  complexe à  *
*  manipuler et qui le réserve donc à des développeurs et des professionnels  *
*  avertis  possédant  des  connaissances  informatiques  approfondies.  Les  *
*  utilisateurs sont   donc invités   à charger   et tester  l'adéquation du  *
*  logiciel à   leurs besoins  dans des  conditions permettant  d'assurer la  *
*  sécurité de leurs systèmes et ou de leurs données et, plus  généralement,  *
*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.         *
*                                                                             *
*  Le fait que   vous puissiez accéder à cet  en-tête signifie que vous avez  *
*  pris connaissance de la licence CeCILL,   et que vous en avez accepté les  *
*  termes.                                                                    *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  This file is part of my educational resources.                             *
*                                                                             *
*  This software is governed by the CeCILL license under French law and       *
*  abiding by the rules of distribution of free software. You can use,        *
*  modify and/ or redistribute the software under the terms of the CeCILL     *
*  license as circulated by CEA, CNRS and INRIA at the following URL          *
*  "http://www.cecill.info".                                                  *
*                                                                             *
*  As a counterpart to the access to the source code and rights to copy,      *
*  modify and redistribute granted by the license, users are provided only    *
*  with a limited warranty and the software's author, the holder of the       *
*  economic rights, and the successive licensors have only limited            *
*  liability.                                                                 *
*                                                                             *
*  In this respect, the user's attention is drawn to the risks associated     *
*  with loading, using, modifying and/or developing or reproducing the        *
*  software by the user in light of its specific status of free software,     *
*  that may mean that it is complicated to manipulate, and that also          *
*  therefore means that it is reserved for developers and experienced         *
*  professionals having in-depth computer knowledge. Users are therefore      *
*  encouraged to load and test the software's suitability as regards their    *
*  requirements in conditions enabling the security of their systems and/or   *
*  data to be ensured and, more generally, to use and operate it in the same  *
*  conditions as regards security.                                            *
*                                                                             *
*  The fact that you are presently reading this means that you have had       *
*  knowledge of the CeCILL license and that you accept its terms.             *
*                                                                             *
******************************************************************************/

#ifndef __TEST_FONCTIONS_H__
#define __TEST_FONCTIONS_H__

/*
 * SEC_t est le type correspondant à une fonction qui
 * prend deux entiers non signés en paramètres et qui
 * renvoie un entier non signé.
 */
typedef unsigned int (*SECG_t)(unsigned int, unsigned int);

/*
 * Structure de donnée permettant de représenter le
 * résultat d'un test de fonction.
 */
typedef struct {
  SECG_t             fonction;   /* Fonction utilisée                     */
  unsigned int       nb_tests;   /* Nombre de tests effectués             */
  unsigned int       param_a;    /* Premier paramètre passé à la fonction */
  unsigned int       param_b;    /* Second paramètre passé à la fonction  */
  unsigned int       resultat;   /* résulat calculé par la fonction       */
  long unsigned int  temps_ms;   /* Temps mesuré en millisecondes         */
  long int           memoire_ko; /* Mémoire mesurée en kilo-octets        */
} infos_t;

/*
 * Fonction de test permettant d'appliquer la fonction
 * fct(a, b) un nombre de fois donné et qui renvoie la valeur
 * calculée ainsi que le temps et la mémoire utilisée.
 */
infos_t test_fonction(SECG_t fct, unsigned int a, unsigned int b, unsigned int nb_tests);

/*
 * Affiche les infos passées en second paramètre sur la sortie standard.
 */
void print_infos(infos_t infos);

/*
 * Renvoie le nom de la fonction passée en paramètre (ou "???" si non trouvé)
 */
const char *get_fct_name(SECG_t fct);

#endif
