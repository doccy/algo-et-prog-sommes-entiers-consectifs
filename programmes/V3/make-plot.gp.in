#!/usr/bin/gnuplot
###############################################################################
#                                                                             #
#  Copyright © 2016-2021 -- DoccY's Production                                #
#                                                                             #
#  Auteurs/Authors: Alban MANCHERON <doccy@mancheron.fr>                      #
#                                                                             #
#  -------------------------------------------------------------------------  #
#                                                                             #
#  Ce fichier fait partie de mes ressources pédagogiques.                     #
#                                                                             #
#  Ce logiciel est régi par la   licence CeCILL soumise au droit français et  #
#  respectant les principes  de diffusion des logiciels libres.  Vous pouvez  #
#  utiliser, modifier et/ou redistribuer ce programme sous les conditions de  #
#  la licence CeCILL telle que diffusée par   le CEA, le CNRS et l'INRIA sur  #
#  le site "http://www.cecill.info".                                          #
#                                                                             #
#  En contrepartie de l'accessibilité au code source et des droits de copie,  #
#  de modification et de redistribution accordés par cette licence, il n'est  #
#  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  #
#  seule une responsabilité  restreinte pèse  sur l'auteur du programme,  le  #
#  titulaire des droits patrimoniaux et les concédants successifs.            #
#                                                                             #
#  À  cet égard  l'attention de  l'utilisateur est  attirée sur  les risques  #
#  associés au   chargement, à   l'utilisation, à  la modification  et/ou au  #
#  développement et   à la reproduction du  logiciel par l'utilisateur étant  #
#  donné  sa spécificité  de logiciel libre,  qui peut le rendre  complexe à  #
#  manipuler et qui le réserve donc à des développeurs et des professionnels  #
#  avertis  possédant  des  connaissances  informatiques  approfondies.  Les  #
#  utilisateurs sont   donc invités   à charger   et tester  l'adéquation du  #
#  logiciel à   leurs besoins  dans des  conditions permettant  d'assurer la  #
#  sécurité de leurs systèmes et ou de leurs données et, plus  généralement,  #
#  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.         #
#                                                                             #
#  Le fait que   vous puissiez accéder à cet  en-tête signifie que vous avez  #
#  pris connaissance de la licence CeCILL,   et que vous en avez accepté les  #
#  termes.                                                                    #
#                                                                             #
#  -------------------------------------------------------------------------  #
#                                                                             #
#  This file is part of my educational resources.                             #
#                                                                             #
#  This software is governed by the CeCILL license under French law and       #
#  abiding by the rules of distribution of free software. You can use,        #
#  modify and/ or redistribute the software under the terms of the CeCILL     #
#  license as circulated by CEA, CNRS and INRIA at the following URL          #
#  "http://www.cecill.info".                                                  #
#                                                                             #
#  As a counterpart to the access to the source code and rights to copy,      #
#  modify and redistribute granted by the license, users are provided only    #
#  with a limited warranty and the software's author, the holder of the       #
#  economic rights, and the successive licensors have only limited            #
#  liability.                                                                 #
#                                                                             #
#  In this respect, the user's attention is drawn to the risks associated     #
#  with loading, using, modifying and/or developing or reproducing the        #
#  software by the user in light of its specific status of free software,     #
#  that may mean that it is complicated to manipulate, and that also          #
#  therefore means that it is reserved for developers and experienced         #
#  professionals having in-depth computer knowledge. Users are therefore      #
#  encouraged to load and test the software's suitability as regards their    #
#  requirements in conditions enabling the security of their systems and/or   #
#  data to be ensured and, more generally, to use and operate it in the same  #
#  conditions as regards security.                                            #
#                                                                             #
#  The fact that you are presently reading this means that you have had       #
#  knowledge of the CeCILL license and that you accept its terms.             #
#                                                                             #
###############################################################################

# Valeurs utilisées dans ce script:
SCRIPT_NAME  = "@SCRIPT_NAME@"
NB_TESTS     = @NB_TESTS@
MIN_N_VALUE  = @MIN_N_VALUE@
MAX_N_VALUE  = @MAX_N_VALUE@
STEP_N_VALUE = @STEP_N_VALUE@
ALGOS        = "@ALGOS@"
OPT          = "@OPT@"

script_valeurs      = (strstrt(SCRIPT_NAME, "valeurs") != 0)
script_performances = (strstrt(SCRIPT_NAME, "performances") != 0)

if (!script_valeurs && !script_performances) {
  print "DBG"
  show variables all
  exit error "Impossible de déterminer le type de graphique à générer"
}

# Par défaut les graphiques sont affichés.
# On change le terminal pour produire des images au format png.
set terminal png notransparent interlace truecolor enhanced nocrop font "Arial,18" size 1600,1200

# Axe des abscisses
set xtics MIN_N_VALUE, STEP_N_VALUE, MAX_N_VALUE
set xlabel "Valeur de {/Helvetica-Oblique n}"

# Légende
set key outside below height 2

# Calcul du nombre d'algos testés
nb_algos = words(ALGOS)

# Titre du graphique
if (script_valeurs) {
  png_file = sprintf("valeurs-O%s.png", OPT)
  titre = "Résultats des algorithmes calculant\n"
} else {
  png_file = sprintf("performances-O%s.png", OPT)
  titre = "Performances des algorithmes calculant\n"
}
titre = titre . sprintf("la somme des entiers consécutifs de %u à %u\n", MIN_N_VALUE, MAX_N_VALUE)
titre = titre . sprintf("(%u exécutions pour chaque algorithme)\n", NB_TESTS)
titre = titre . sprintf("[compilation avec l'option -O%s]", OPT)
set title titre

# Calcul de la liste des fichiers d'entrée.
files = ""
do for [i=1:nb_algos] {
  files = files . sprintf(" res-%s-O%s.csv", word(ALGOS, i), OPT)
}

# Nom du fichier de sortie
set output png_file

if (script_valeurs) {

  # Axe des ordonnées
  set ylabel "Somme calculée"
  set format y "%.1t{/Symbol \264}10^{%T}"

  # Agrandissement de la marge droite (5% de la largeur totale)
  set rmargin 5

  # Affichage du label 2^{32} sur la marge droite
  set label at graph 1, first 2**32 left textcolor "#CC4400" offset 1, 0 "2^{32}"

  # Affichage du label 2^{16} sur l'axe des absisses
  set label at 2**16, graph 0 center textcolor "#CC4400" offset 0, -1 "2^{16}"

  # Affichage d'une ligne verticale croisant l'axe des abscisses à la valeur 2^{16}
  set arrow nohead from 2**16, graph 0 rto 0, graph 1 linetype 0 linecolor "#CC4400"

  # Création du graphique
  plot for [i=1:nb_algos] word(files,i) using 2:4 with linespoints pointsize (nb_algos-i+1) title sprintf("Algo %s", word(ALGOS, i)),\
       2**32 linetype 0 linecolor "#CC4400" notitle

} else {

  # Axe des ordonnées de gauche
  set ylabel "Temps (en sec.)"
  set yrange [-0.125:4]
  set ytics nomirror

  # Axe des ordonnées de droite
  set y2label "Mémoire"
  set y2range [-64*1024:2*1024**2]
  set y2tics 0,1024*1024
  set my2tics
  set format y2 "%.0b%BB"

  # Légende sur deux colonnes
  set key maxcols 2

  # Création du graphique
  plot for [i=1:nb_algos] word(files,i) using 2:($5/1000) with lines linecolor i title sprintf("Temps %s", word(ALGOS, i)), \
       for [i=1:nb_algos] word(files,i) using 2:($6*1024) with linespoints linecolor i pointtype 6 pointsize 1 title sprintf("Mémoire %s", word(ALGOS, i)) axes x1y2
}
