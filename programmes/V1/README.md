---
title: Calcul d'entiers consécutifs

subtitle: Version 1 des programmes

author: Alban Mancheron

lang: fr-FR

---

Ceci est une version du programme (écrit en C) permettant de tester
plusieurs fonctions permettant de calculer la somme d'entiers
consécutifs.

# Les fichiers C

Le fichier [`fonctions.h`](./fonctions.h) contient la signature des
trois fonctions implémentant trois algorithmes distincts permettant de
calculer la somme des $n$ premiers entiers consécutifs (donc $0 + 1 +
2 + ... + n$). Le premier algorithme est l'algorithme naïf, le second
l'algorithme récursif et le troisième celui exploitant la formule
close correspondant à cette somme (les implémentations sont dans le
fichier [`fonctions.c`](./fonctions.c)).

Le fichier [`test_fonctions.h`](./test_fonctions.h) contient les
définitions des fonctions permettant de tester les différentes
stratégies (les implémentations sont dans le fichier
[`test_fonctions.c`](./test_fonctions.c)).

Le fichier [`test_sommes.c`](./test_sommes.c) implémente le programme
de test.

Ces codes correspondent aux sections 1.2 et 2.1 de l'article.

# Les fichiers scripts

Le fichier [`make-plot-performances.gp`](./make-plot-performances.gp)
est un script [gnuplot](http://www.gnuplot.info) permettant
d'afficher les consommations en temps et en mémoire de chacune des
trois implémentations.

Le fichier [`make-plot-valeurs.gp`](./make-plot-valeurs.gp) est un
script [gnuplot](http://www.gnuplot.info) permettant d'afficher les
résultats calculés par chacune des trois implémentations.

Le fichier
[`make-plot-performances2.gp`](./make-plot-performances2.gp) est un
script [gnuplot](http://www.gnuplot.info) permettant d'afficher les
consommations en temps et en mémoire de chaque implémentation pour les
différents niveaux d'optimisation.

Ces codes correspondent (globalement) aux codes de la section 2.2 de
l'article.

# L'artillerie lourde

Le fichier [`make-plot.gp.in`](./make-plot.gp.in) est un modèle de
script [gnuplot](http://www.gnuplot.info) permettant de créer des
scripts permettant soit d'afficher les consommations en temps et en
mémoire soit les valeurs calculées de chacune des trois
implémentations, et cela pour un niveau donné d'optimisation du
compilateur.

Le fichier [`Makefile`](./Makefile) permet de lancer l'artillerie
lourde. Il reprend les bases de la version 0 mais ajoute la production
des données de test et la génération des graphiques pour chaque niveau
d'optimisation du compilateur.

Ce fichier propose les cibles génériques suivantes :

- `all` : cible par défaut qui génère tous les programmes, calcule les
  données de test et génère tous les graphiques;
  
- `prog` : crée l'exécutable pour le niveau d'optimisation donné par
  la variable `OPT`;

- `csv` : teste les implémentations compilées avec le niveau
  d'optimisation donné par la variable `OPT`;

- `png` : génère les graphiques à partir des données produites lors
  des tests pour le niveau d'optimisation donné par la variable `OPT`;

- `mostlyclean` : fait le ménage pour le niveau d'optimisation donné
  par la variable `OPT`;

- `clean` : fait le ménage pour tous les niveaux d'optimisation;

- `dist` : crée une archive distribuable contenant le code.

