#!/usr/bin/gnuplot

# Valeurs utilisées dans ce script:
NB_TESTS     = 10000
MIN_N_VALUE  = 0
MAX_N_VALUE  = 100000
STEP_N_VALUE = 10000
ALGOS        = "Iter Rec Math"

# Par défaut les graphiques sont affichés.
# On change le terminal pour produire des images au format png.
set terminal png notransparent interlace truecolor enhanced nocrop font "Arial,18" size 1600,1200

# Axe des abscisses
set xtics MIN_N_VALUE, STEP_N_VALUE, MAX_N_VALUE
set xlabel "Valeur de {/Helvetica-Oblique n}"

# Légende
set key outside below height 2

# Calcul du nombre d'algos testés
nb_algos = words(ALGOS)

# Titre du graphique
titre = "Résultats des algorithmes calculant\n"
titre = titre . sprintf("la somme des entiers consécutifs de %u à %u\n", MIN_N_VALUE, MAX_N_VALUE)
titre = titre . sprintf("(%u exécutions pour chaque algorithme)\n", NB_TESTS)
set title titre

# Calcul de la liste des fichiers d'entrée.
files = ""
do for [i=1:nb_algos] {
  files = files . sprintf(" res-%s.csv", word(ALGOS, i))
}

# Nom du fichier de sortie
set output "valeurs.png"

# Axe des ordonnées
set ylabel "Somme calculée"
set format y "%.1t{/Symbol \264}10^%T"

# Agrandissement de la marge droite (5% de la largeur totale)
set rmargin 5

# Affichage du label 2^{32} sur la marge droite
set label at graph 1, first 2**32 left textcolor "#CC4400" offset 1, 0 "2^{32}"

# Affichage du label 2^{16} sur l'axe des absisses
set label at 2**16, graph 0 center textcolor "#CC4400" offset 0, -1 "2^{16}"

# Affichage d'une ligne verticale croisant l'axe des abscisses à la valeur 2^{16}
set arrow nohead from 2**16, graph 0 rto 0, graph 1 linetype 0 linecolor "#CC4400"

# Création du graphique
plot for [i=1:nb_algos] word(files,i) using 2:3 with linespoints pointsize (nb_algos-i+1) title sprintf("Algo %s", word(ALGOS, i)),\
     2**32 linetype 0 linecolor "#CC4400" notitle
