/******************************************************************************
*                                                                             *
*  Copyright © 2016-2021 -- DoccY's Production                                *
*                                                                             *
*  Auteurs/Authors: Alban MANCHERON <doccy@mancheron.fr>                      *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  Ce fichier fait partie de mes ressources pédagogiques.                     *
*                                                                             *
*  Ce logiciel est régi par la   licence CeCILL soumise au droit français et  *
*  respectant les principes  de diffusion des logiciels libres.  Vous pouvez  *
*  utiliser, modifier et/ou redistribuer ce programme sous les conditions de  *
*  la licence CeCILL telle que diffusée par   le CEA, le CNRS et l'INRIA sur  *
*  le site "http://www.cecill.info".                                          *
*                                                                             *
*  En contrepartie de l'accessibilité au code source et des droits de copie,  *
*  de modification et de redistribution accordés par cette licence, il n'est  *
*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  *
*  seule une responsabilité  restreinte pèse  sur l'auteur du programme,  le  *
*  titulaire des droits patrimoniaux et les concédants successifs.            *
*                                                                             *
*  À  cet égard  l'attention de  l'utilisateur est  attirée sur  les risques  *
*  associés au   chargement, à   l'utilisation, à  la modification  et/ou au  *
*  développement et   à la reproduction du  logiciel par l'utilisateur étant  *
*  donné  sa spécificité  de logiciel libre,  qui peut le rendre  complexe à  *
*  manipuler et qui le réserve donc à des développeurs et des professionnels  *
*  avertis  possédant  des  connaissances  informatiques  approfondies.  Les  *
*  utilisateurs sont   donc invités   à charger   et tester  l'adéquation du  *
*  logiciel à   leurs besoins  dans des  conditions permettant  d'assurer la  *
*  sécurité de leurs systèmes et ou de leurs données et, plus  généralement,  *
*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.         *
*                                                                             *
*  Le fait que   vous puissiez accéder à cet  en-tête signifie que vous avez  *
*  pris connaissance de la licence CeCILL,   et que vous en avez accepté les  *
*  termes.                                                                    *
*                                                                             *
*  -------------------------------------------------------------------------  *
*                                                                             *
*  This file is part of my educational resources.                             *
*                                                                             *
*  This software is governed by the CeCILL license under French law and       *
*  abiding by the rules of distribution of free software. You can use,        *
*  modify and/ or redistribute the software under the terms of the CeCILL     *
*  license as circulated by CEA, CNRS and INRIA at the following URL          *
*  "http://www.cecill.info".                                                  *
*                                                                             *
*  As a counterpart to the access to the source code and rights to copy,      *
*  modify and redistribute granted by the license, users are provided only    *
*  with a limited warranty and the software's author, the holder of the       *
*  economic rights, and the successive licensors have only limited            *
*  liability.                                                                 *
*                                                                             *
*  In this respect, the user's attention is drawn to the risks associated     *
*  with loading, using, modifying and/or developing or reproducing the        *
*  software by the user in light of its specific status of free software,     *
*  that may mean that it is complicated to manipulate, and that also          *
*  therefore means that it is reserved for developers and experienced         *
*  professionals having in-depth computer knowledge. Users are therefore      *
*  encouraged to load and test the software's suitability as regards their    *
*  requirements in conditions enabling the security of their systems and/or   *
*  data to be ensured and, more generally, to use and operate it in the same  *
*  conditions as regards security.                                            *
*                                                                             *
*  The fact that you are presently reading this means that you have had       *
*  knowledge of the CeCILL license and that you accept its terms.             *
*                                                                             *
******************************************************************************/

#include "test_fonctions.h"
#include "fonctions.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>

infos_t test_fonction(SEC_t fct, unsigned int n, unsigned int nb_tests) {
  /* Structure qui sera renvoyée par la fonction */
  infos_t res;

  /* Structures de récupération de l'état des
     ressources avant et après test */
  struct rusage usage_debut, usage_fin;

  /* Initialisation des informations */
  res.fonction = fct;
  res.nb_tests = 0;
  res.parametre = n;
  res.resultat = 0;
  res.temps_ms = 0;
  res.memoire_ko = 0;

  /* Une vérification qui ne coûte pas cher */
  assert(nb_tests > 0);

  /* État des ressources avant les tests */
  if (getrusage(RUSAGE_SELF, &usage_debut)) {
    perror("test_fonction");
    exit(1);
  }

  /* Appel de la fonction le nombre de fois demandé */
  for (res.nb_tests = 0; res.nb_tests < nb_tests; ++res.nb_tests) {
    res.resultat = fct(res.parametre);
  }

  /* État des ressources après les tests */
  if (getrusage(RUSAGE_SELF, &usage_fin)) {
    perror("test_fonction");
    exit(1);
  }

  /* Calcul du temps passé en millisecondes */
  res.temps_ms = usage_fin.ru_utime.tv_sec * 1000;
  res.temps_ms += usage_fin.ru_utime.tv_usec / 1000;
  res.temps_ms -= usage_debut.ru_utime.tv_sec * 1000;
  res.temps_ms -= usage_debut.ru_utime.tv_usec / 1000;

  /* Calcul de la mémoire consommée en kilo-octets */
  res.memoire_ko = usage_fin.ru_maxrss - usage_debut.ru_maxrss;

  return res;
}

void print_infos(infos_t infos) {
  printf("#Fonction\tn\tresultat\ttemps(ms)\tmemoire(ko)\tnb_tests\n");
  printf("%s\t%u\t%u\t%lu\t%lu\t%u\n",
         get_fct_name(infos.fonction), infos.parametre, infos.resultat,
         infos.temps_ms, infos.memoire_ko, infos.nb_tests);
}

const char * get_fct_name(SEC_t fct) {
  if (fct == &SommeEntiersConsecutifsIterative) return "Iterative";
  if (fct == &SommeEntiersConsecutifsRecursive) return "Recursive";
  if (fct == &SommeEntiersConsecutifsMath) return "Math";
  return "???";
}
