#!/usr/bin/gnuplot --persist

# Valeurs utilisées dans ce script:
NB_TESTS     = 10000
MIN_N_VALUE  = 0
MAX_N_VALUE  = 100000
STEP_N_VALUE = 10000
ALGOS        = "Iter Rec Math"
OPTS         = "0 1 2 3"

# Par défaut les graphiques sont affichés.
# On change le terminal pour produire des images au format png.
set terminal png notransparent interlace truecolor enhanced nocrop font "Arial,18" size 1600,1200

# Axe des abscisses
set xtics MIN_N_VALUE, STEP_N_VALUE, MAX_N_VALUE
set xlabel "Valeur de {/Helvetica-Oblique n}"

# Légende
set key outside below height 2

# Calcul du nombre d'algos testés
nb_algos = words(ALGOS)

# Calcul du nombre d'optimisations testées
nb_opts = words(OPTS)

do for [i=1:nb_algos] {
  # Titre du graphique
  algo = word(ALGOS, i)
  titre = sprintf("Performances de l'algorithme %s calculant\n", algo)
  titre = titre . sprintf("la somme des entiers consécutifs de %u à %u\n", MIN_N_VALUE, MAX_N_VALUE)
  titre = titre . sprintf("(%u exécutions pour chaque niveau d'opimisation)\n", NB_TESTS)
  set title titre

  # Calcul de la liste des fichiers d'entrée.
  files = ""
  do for [j=1:nb_opts] {
    files = files . sprintf(" res-%s-O%s.csv", algo, word(OPTS, j))
  }

  # Nom du fichier de sortie
  set output sprintf("performances-%s.png", algo)

  # Axe des ordonnées de gauche
  set ylabel "Temps (en sec.)"
  set yrange [-0.125:4]
  set ytics nomirror

  # Axe des ordonnées de droite
  set y2label "Mémoire"
  set y2range [-64*1024:2*1024**2]
  set y2tics 0,1024*1024
  set my2tics
  set format y2 "%.0b%BB"

  # Légende sur deux colonnes
  set key maxcols 2

  # Création du graphique
  plot for [j=1:nb_opts] word(files,j) using 2:($4/1000) with lines linecolor j title sprintf("Temps O%s", word(OPTS, j)), \
       for [j=1:nb_opts] word(files,j) using 2:($5*1024) with linespoints linecolor j pointtype 6 pointsize 1 title sprintf("Mémoire O%s", word(OPTS, j)) axes x1y2
}
