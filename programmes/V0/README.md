---
title: Calcul d'entiers consécutifs

subtitle: Version 0 des programmes

author: Alban Mancheron

lang: fr-FR

---

Ceci est la version préliminaire du programme (écrit en C) permettant
de calculer la somme d'entiers consécutifs.

Ici, un seul fichier de code: le fichier
[`test_sommes.c`](./test_sommes.c) qui contient la description d'un
algorithme naïf permettant de calculer la somme des $n$ premiers
entiers consécutifs (donc $0 + 1 + 2 + ... + n$).

Ce code correspond à la section 1.1 de l'article.

Le fichier [`Makefile`](./Makefile) peut paraître compliqué pour
simplement générer un exécutable à partir d'un fichier mais il sera
étoffé dans les versions suivantes.

Ce fichier propose les cibles génériques suivantes :

- `all` : cible par défaut qui appelle la cible `prog`;

- `prog` : crée l'exécutable `test_sommes`;

- `clean` : fait le ménage;

- `dist` : crée une archive distribuable contenant le code.

