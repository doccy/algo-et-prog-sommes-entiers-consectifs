---
title: Calcul d'entiers consécutifs

subtitle: Programmes présentés dans l'article

author: Alban Mancheron

lang: fr-FR

---

Les codes proposés correspondent à [l'article sur l'algorithmique et
la programmation publié dans le numéro 255 de la revue *GNU/Linux
Magazine France* de janvier/février
2022](https://connect.ed-diamond.com/gnu-linux-magazine/glmf-255/entre-algorithmique-et-programmation).

Les programmes des sous-répertoires correspondent à l'évolution du
programme (écrit en C) permettant de tester plusieurs fonctions
permettant de calculer la somme d'entiers consécutifs.

Chaque sous-répertoire est indépendant, mais il est intéressant de
comparer deux sous-répertoires consécutifs afin d'observer les
évolutions proposées.

Le sous-repertoire [`V0`](./V0) correspond à la section 1.1 de
l'article.

Le sous-repertoire [`V1`](./V1) correspond à partie commençant à la
section 1.2 jusqu'à la partie 2.2 de l'article.

Le sous-repertoire [`V2`](./V2) correspond à la section 2.3 de
l'article.

Le sous-repertoire [`V3`](./V3) correspond à la section 3 de
l'article.
