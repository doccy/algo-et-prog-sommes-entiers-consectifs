---
title: Calcul d'entiers consécutifs

subtitle: Ressources (images) de l'article

author: Alban Mancheron

lang: fr-FR

---

Les images de ce répertoire correspondent à celles utilisées dans
[l'article sur l'algorithmique et la programmation publié dans le
numéro 255 de la revue *GNU/Linux Magazine France* de janvier/février
2022](https://connect.ed-diamond.com/gnu-linux-magazine/glmf-255/entre-algorithmique-et-programmation).

Les images `formule_*.png` ont été générées à partir de forumles
écrites en LaTeX.

Les images `gnuplot_*png` ont été générées *via* l'outil `gnuplot`
principalement à partir des scripts présents dans le sous-répertoires
[`programmes/V1`](../programmes/V1).
